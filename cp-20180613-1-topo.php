<?php //EMKT Clientes de Troca ?>

<p class="fs-16 text-center nunito-bold mt-4">
	Sua oportunidade de trocar de smartphone é agora. <br>
	A Vivo está com <span style="color: #652c90;">desconto de até R$360</span> no aparelho!
</p>
<figure>
	<img src="assets/img/sansung-a8-s8.png" class="img-fluid d-block mx-auto">
	<figcaption>
		<h3 class="nunito-black color-roxo fs-8 text-center mt-3">
			<span style="color: #652c90;">Samsung S8 e Samasung A8</span>
		</h3>
	</figcaption>
</figure>