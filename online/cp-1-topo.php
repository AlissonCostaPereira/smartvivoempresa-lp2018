<p class="fs-16 text-center nunito-bold mt-4">
	Traga seu número de celular para Vivo e<br/>
	<span class="color-roxo">desbloqueie um desconto</span> de até R$720 em aparelho!
</p>
<figure>
	<img src="assets/img/celulares.png" class="img-fluid d-block mx-auto">
	<figcaption>
		<h3 class="nunito-black color-roxo fs-8 text-center mt-3">
			Samsung Galaxy S8 e Samsung A8 
		</h3>
	</figcaption>
</figure>