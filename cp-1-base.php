<p class="nunito-black fs-14 text-center mt-4 line-height">
	Plano Smart Empresa Ilimitado + 7GB 4GPlus<br/>
	+ Samsung A8, por: <span class="color-roxo">R$149,99/mês</span>.
</p>
<p class="nunito-black fs-14 text-center mt-3 line-height">
	Plano Smart Empresa Ilimitado + 10GB 4GPlus<br/>
	+ Samsung Galaxy S8 64GB, por: <span class="color-roxo">R$189,99/mês</span>.
</p>

<h2 class="color-roxo nunito-black fs-14 text-center mb-3">
	Sem surpresa na conta
</h2>

<p class="nunito-bold fs-12 text-center mb-5 line-height">
	Você fala o quanto quiser com fixo e móvel de qualquer operadora do Brasil e<br/>
	navegue à vontade com 7GB ou 10GB 4GPlus.<br/>
	Samsung A8, por: 24xR$50 ou Samsung Galaxy S8 64GB, por: 24xR$80.
</p>