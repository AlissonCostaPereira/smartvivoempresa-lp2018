<?php //EMKT Oferta de Migração ?>

<p class="fs-16 text-center nunito-bold mt-4">
	<b>A Vivo se atualizou e agora traz para voc&ecirc;<br/>
	novas oportunidades, conhe&ccedil;a o <span style="color: #652c90;">Smart Empresa.</span>
	<br/><br/>
	<span style="color: #652c90;font-weight: 900;" >Sem surpresa na conta</span><br/>
	<b style="font-size: 12px;">Liga&ccedil;oes ilimitadas nacionais e para as Am&eacute;ricas, di&aacute;rias Vivo Travel,<br/>
	7GB dados para usar como quiser e ainda um smartphone novo!</b>
</p>
<figure>
	<img src="assets/img/sansung-a8-s8.png" class="img-fluid d-block mx-auto">
	<figcaption>
		<h3 class="nunito-black color-roxo fs-8 text-center mt-3">
			Samsung Galaxy S8 e Samsung A8
		</h3>
	</figcaption>
</figure>