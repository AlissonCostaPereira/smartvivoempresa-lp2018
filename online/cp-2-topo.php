<p class="fs-16 text-center nunito-bold mt-4">
	J&aacute; pensou contratar um plano de celular com voz ilimitada<br/>
	um Smartphone por um pre&ccedil;o acess&iacute;vel?
</p>
<figure>
	<img src="assets/img/motozplay.png" class="img-fluid d-block mx-auto">
	<figcaption>
		<h3 class="nunito-black color-roxo fs-8 text-center mt-3">
			Moto Z2 Play
		</h3>
	</figcaption>
</figure>