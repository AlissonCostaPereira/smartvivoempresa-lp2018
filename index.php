<?php
$origem = isset($_GET["utm_source"]) ? $_GET["utm_source"] : "";

$campanhas = array(
	'5ac287-bec56-d3afa182ba-92b68c-2352d' => array('include' => 'cp-1', 'produtos' => array('Smart Empresa Ilimitado + 7GB 4GPlus + Samsung A8', 'Smart Empresa Ilimitado + 10GB 4GPlus + Samsung Galaxy S8 64GB' )),
	'86c15-9981e-8445b2-ab26b2-d9aeebd816' => array('include' => 'cp-2', 'produtos' => array('Moto Z2 Play ')),
	'0d1cf62287-efab2-9b5742b-a5af2-43db4' => array('include' => 'cp-3', 'produtos' => array('Samsung A8')),
	'9f924-f57fe46c01-c98b0-4ea96c3-a8219' => array('include' => 'cp-4', 'produtos' => array('Samsung Galaxy S8 64GB')),
	'7135-fba-a27d4-0c733e-645c6-07572-ae6c' => array('include' => 'cp-20180613-1', 'produtos' => array('Smart Empresa Ilimitado + 7GB 4GPlus + Samsung A8','Smart Empresa Ilimitado + 7GB 4GPlus + Samsung S8 64GB')), // EMKT Clientes de Troca
	'7810-d8370-a568-40922-2f06-cff5c-752e1' => array('include' => 'cp-20180613-2', 'produtos' => array('Smart Empresa Ilimitado + 7GB 4GPlus + Samsung A8','Smart Empresa Ilimitado + 10GB 4GPlus + Samsung Galaxy S8 64GB')), // EMKT Oferta de Voz
	'35265-7b91-58e9-2e10c-9724-0f21c-47e62' => array('include' => 'cp-20180613-3', 'produtos' => array('Smart Empresa Ilimitado + 7GB 4GPlus + Samsung A8','Smart Empresa Ilimitado + 7GB 4GPlus + Samsung S8 64GB')), //EMKT Oferta de Migração
);

if (!array_key_exists($origem, $campanhas)) {
	echo "campanha n&atilde;o identificada";
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119618973-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119618973-1');
	</script>

</head>
<body> 
	<header>
		<div class="wrapper">
			<figure class="pt-4 ml-5 float-left">
				<img src="assets/img/vivo.png"> 
			</figure>
			<figure class="pt-4 mt-3 mr-5 float-right">
				<img src="assets/img/smart.png"> 
			</figure>
		</div>
	</header>
	<section class="mt-5 pt-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="nunito-bold text-center linha-roxa fs-24">
						Olá,
					</h1>
					<?php
					include($campanhas[$origem]['include']."-topo.php");
					?>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<form class="formulario" name="formulario" action="dados.php" method="POST">
						<input type="hidden" name="origem" value="<?php echo $origem; ?>" />
						<h2 class="text-center fs-24 nunito-bold mt-4">
							Informe seus dados<br/>
							<small class="color-roxo fs-10 nunito-black">
								*todos os dados são requeridos.
							</small>
						</h2>
						<div class="row" style="max-width:302px;margin:auto;" >
							<div class="form-group">
								<select name="promocao" class="custom-select input-width color-cinza nunito-bold">
									<option value="Não selecionado" selected>Escolha a promo&ccedil;&atilde;o</option>
									<?php 
									foreach ($campanhas[$origem]['produtos'] as $value) {
										echo '<option value="'.$value.'">'.$value.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label class="subir-nome">
									Seu Nome
								</label>
								<input type="text" name="nome" class="form-control input-width">
							</div>
							<div class="form-group">
								<label class="subir-nome">
									Seu E-mail
								</label>
								<input type="text" name="email" class="form-control input-width">
								
							</div>
							<div class="form-group">
								<label class="subir-nome">
									Seu Telefone
								</label>
								<input type="text" name="telefone" class="form-control input-width">
							</div>
							<div class="form-group">
								<div class="g-recaptcha" data-sitekey="6Lf-j1gUAAAAAJ_QP9cfVznLbAzDQH5eHQsTRlgH"></div>
							</div>
							<div class="form-group mt-4">
								<input type="hidden" name="camp-verificar">
								<button type="button" name="enviar" class="fs-14 text-white nunito-black text-uppercase bnt-envio" onclick="validar();">
									Garantir meu desconto
								</button>
							</div>
						</div>
					</form>
					<div class="preloader mt-5 pb-5 mb-5 pt-5">
						<h3 class="fs-12 text-center mt-5 nunito-black">
							Aguarde, estamos registrando seus dados...
						</h3>
						<figure>
							<img src="assets/img/preloader.gif" class="d-block mx-auto img-fluid">
						</figure>
					</div>
					<div class="sucesso mt-5 pb-5 mb-5 pt-5">
						<h3 class="fs-24 text-center mt-5 color-roxo nunito-black">
							Obrigado!
						</h3>
						<p class="fs-14 text-center nunito-black">
							Seus dados foram registrados<br/>
							com sucesso
						</p>
						<figure>
							<img src="assets/img/sucesso.png" class="d-block mx-auto img-fluid">
						</figure>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<?php include($campanhas[$origem]['include']."-base.php"); ?>
					
					<div class="contato nunito-black fs-12 text-center">
						<a href="http://www.smartvivoempresa.com.br/" class="nav-link text-white" >
							www.smartvivoempresa.com.br
						</a>
						<span class="text-white">
							0800 608 1128 | 4007-2962
						</span>
					</div>

				</div>
			</div>
		</div>
	</section>
	<footer class="mt-2">
		<p class="fs-8 nunito-black color-roxo text-center mb-1">
			Av. Senador V&iacute;rgilio Tavora, 150, Sala 701, Fortaleza/CE  |  Av. Rio Grande do Sul, 1101, Jo&atilde;o Pessoa/PB
		</p>
		<p class="fs-8 nunito-black color-roxo text-center mb-1">
			Av. Coronel Joaquim Manoel, 717, Sala 813, Natal/RN  |  R. Tupinamb&aacute;s, 234, Recife/PE
		</p>
	</footer>
	<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>