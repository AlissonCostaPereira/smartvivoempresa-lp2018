<p class="fs-16 text-center nunito-bold mt-4">
	Troque seu aparelho antigo por um Smartphone novinho!<br/>
</p>
<figure>
	<img src="assets/img/S8.png" class="img-fluid d-block mx-auto">
	<figcaption>
		<h3 class="nunito-black color-roxo fs-8 text-center mt-3">
			Samsung Galaxy S8 64GB
		</h3>
	</figcaption>
</figure>