$(".input-width").focus(function(){$(this).parent().find("label").addClass("focus")});
$(".input-width").blur(function(){""==$(this).val()&&$(this).parent().find("label").removeClass("focus")});


function validar(){
	
	var nome	 = formulario.nome.value;
	var	email    = formulario.email.value;
	var	telefone = formulario.telefone.value;
	var enviar   = true;

	if (nome == "") {

		alert("preencha os campos!");
		formulario.nome.focus();
		enviar = false
	}
	else if (email == "") {

		alert("preencha os campos!");
		formulario.email.focus();
		enviar = false
	}
	else if (telefone == "") {

		alert("preencha os campos!");
		formulario.telefone.focus();
		enviar = false
	}
	if (document.getElementById('g-recaptcha-response') == null){

		enviar = false;
		alert("Preencha os campos abaixo corretamente e marque o reCAPTCHA para prosseguir.");

	}else if(document.getElementById('g-recaptcha-response').value == ""){
		enviar = false;
		alert("Preencha os campos abaixo corretamente e marque o reCAPTCHA para prosseguir.");
	}
	
	console.log(enviar);

	if(enviar == true){
		
		console.log(enviar);
		
		$(".formulario").fadeOut();
		$(".preloader").fadeIn();
		
		$.ajax({
			method: "POST",
			url: "dados.php",
			cache: false,
			data: $('.formulario').serialize()
		})
		.done(function(msg) {
			
			console.log("ok");

			$(".preloader").fadeOut();
			$(".sucesso").fadeIn();

			$(".formulario")[0].reset();
			grecaptcha.reset();

			setTimeout(function(){
				$(".sucesso").fadeOut();
				$(".formulario").fadeIn();
			},3000);

		})
		.fail(function( jqXHR, textStatus, errorThrown ){
			alert('Não foi possível completar seu cadastro, tente novamente mais tarde, se o problema persistir entre em contato.')
		});
	}
}